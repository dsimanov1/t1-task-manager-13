package ru.t1.simanov.tm.api.controller;

import ru.t1.simanov.tm.model.Task;

import java.util.List;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void renderTasks(List<Task> tasks);

    void showTaskByProjectId();

    void showTask(Task task);

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void findTasksByProjectId();

}
