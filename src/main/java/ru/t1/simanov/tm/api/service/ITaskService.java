package ru.t1.simanov.tm.api.service;

import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllTasksByProjectId(String projectId);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
