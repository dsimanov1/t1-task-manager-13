package ru.t1.simanov.tm.api.repository;

import ru.t1.simanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllTasksByProjectId(String projectId);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Integer getSize();

    List<Task> findAllByProjectId(String projectId);

}
