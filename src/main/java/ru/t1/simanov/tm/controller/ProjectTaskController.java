package ru.t1.simanov.tm.controller;

import ru.t1.simanov.tm.api.controller.IProjectTaskController;
import ru.t1.simanov.tm.api.service.IProjectTaskService;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class ProjectTaskController implements IProjectTaskController {

    public final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
    }

}
